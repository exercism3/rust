#[derive(Debug, PartialEq, Eq)]
pub struct Dna(String);

#[derive(Debug, PartialEq, Eq)]
pub struct Rna(String);

const DNA_SET: &str = "GCTA";
const RNA_SET: &str = "GCAU";

impl Dna {
    pub fn new(dna: &str) -> Result<Dna, usize> {
        if let Some(i) = dna.chars().position(|ch| !DNA_SET.contains(ch)) {
            return Err(i);
        }
        Ok(Dna(dna.to_string()))
    }

    pub fn into_rna(self) -> Rna {
        Rna(self.0.chars().map(|ch| match ch {
            'G' => 'C',
            'C' => 'G',
            'T' => 'A',
            'A' => 'U',
            _ => unreachable!(),
        }).collect())
    }
}

impl Rna {
    pub fn new(rna: &str) -> Result<Rna, usize> {
        if let Some(i) = rna.chars().position(|ch| !RNA_SET.contains(ch)) {
            return Err(i);
        }
        Ok(Rna(rna.to_string()))
    }
}
