use std::collections::HashMap;
use std::thread;

type FrequencyMap = HashMap<char, usize>;

pub fn frequency(input: &[&str], worker_count: usize) -> FrequencyMap {
    // todo!(
    //     "Count the frequency of letters in the given input '{input:?}'. Ensure that you are using {} to process the input.",
    //     match worker_count {
    //         1 => "1 worker".to_string(),
    //         _ => format!("{worker_count} workers"),
    //     }
    // );

    // Split workload
    let chuck_size = if input.len() % worker_count == 0 && input.len() > 0 {
        input.len() / worker_count
    } else {
        input.len() / worker_count + 1
    };

    let chunks: Vec<Vec<String>> = input.chunks(chuck_size)
        .map(|chunk| chunk.iter().map(|s| s.to_string()).collect())
        .collect();

    // spawn thread and work (map)
    let mut workers = vec![];
    for chunk in chunks {
        workers.push(thread::spawn(move || {
            frequency_sequential(&chunk)
        }))
    }

    // aggregate results (reduce)
    let mut aggregated_result: FrequencyMap = HashMap::new();
    for worker in workers {
        let worker_result: FrequencyMap = worker.join().unwrap();
        for (&char, &count) in worker_result.iter() {
            *aggregated_result.entry(char).or_insert(0) += count;
        }
    }
    aggregated_result
}

pub fn frequency_sequential(lines: &[String]) -> FrequencyMap {
    let mut counts = HashMap::new();
    for line in lines {
        for c in line.to_lowercase().chars()
            .filter(|c| c.is_alphabetic()) {
            *counts.entry(c).or_insert(0) += 1;
        }
    }
    counts
}
