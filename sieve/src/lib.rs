pub fn primes_up_to(upper_bound: u64) -> Vec<u64> {
    (2..)
        .take(upper_bound as usize - 1)
        .filter(|&n| (2..(n as f64).sqrt() as u64 + 1)
            .all(|i| n % i != 0))
        .collect()
}
