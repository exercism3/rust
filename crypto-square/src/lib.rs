pub fn encrypt(input: &str) -> String {
    let normalized = 
        input
            .chars()
            .filter(char::is_ascii_alphanumeric)
            .map(|c| c.to_ascii_lowercase())
            .collect::<Vec<_>>();

    let width = (normalized.len() as f64).sqrt().ceil() as usize;

    (0..width)
            .map(|col| normalized.chunks(width)
                .map(|v| v.get(col).unwrap_or(&' '))
                .collect::<String>())
            .collect::<Vec<_>>()
            .join(" ")
}
