pub fn rotate(input: &str, key: u8) -> String {
    input
        .chars()
        .map(|c| match c {
            ('a'..='z') | ('A'..='Z') => {
                let base = if c.is_ascii_lowercase() { b'a' } else { b'A' };
                (((c as u8 - base + key) % 26) + base) as char
            }
            _ => c,
        })
        .collect()
}
