pub type Value = i32;
pub type ForthResult = std::result::Result<(), Error>;

type Operation = fn(&mut Forth) -> ForthResult;
#[derive(Default)]
pub struct Forth {
    stack: Vec<Value>,
    words: Vec<(String, Vec<String>)>
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    DivisionByZero,
    StackUnderflow,
    UnknownWord,
    InvalidWord,
}

impl Forth {
    pub fn new() -> Forth {
        Default::default()
    }

    pub fn stack(&self) -> Vec<Value>{
        self.stack.clone()
    }

    pub fn eval(&mut self, input: &str) -> ForthResult {
        let tokens = Forth::tokenize(input);
        self.eval_tokens(&tokens)
    }

    fn tokenize(input: &str) -> Vec<String> {
        input.chars()
            .collect::<Vec<_>>()
            .split(|c| c.is_control() || c.is_whitespace())
            .map(|s| s.iter().collect::<String>())
            .collect()
    }

    fn eval_tokens(&mut self, tokens: &[String]) -> ForthResult {
        let mut result = Ok(());
        if let Some((token, others)) = tokens.split_first() {
            if token == ":" {
                result = self.define_word(others);
            } else {
                result = self.evaluate(tokens, self.words.len());
            }
        }
        result
    }

    fn define_word(&mut self, tokens: &[String]) -> ForthResult {
        if let Some((word, others)) = tokens.split_first() {
            if word.parse::<i32>().is_ok() || !others.contains(&";".to_string()) {
                Err(Error::InvalidWord)
            } else {
                let mut words = others.splitn(2, |t| t == ";");
                self.words.push((word.to_lowercase(), words.next().unwrap().to_vec()));
                if let Some(tail) = words.next() {
                    self.eval_tokens(tail)
                } else {
                    Ok(())
                }
            }
        } else {
            Err(Error::InvalidWord)
        }
    }

    fn evaluate(&mut self, tokens: &[String], available_words: usize) -> ForthResult {
        let mut result = Ok(());

        if let Some((token, others)) = tokens.split_first() {
            if let Ok(num) = token.parse::<i32>() {
                self.stack.push(num);
            } else if let Some(rev_index) = self.words[..available_words]
                .iter().rev().position(|w| w.0 == token.to_lowercase()) {
                let index = available_words - rev_index - 1;
                result = self.evaluate(&self.words.get(index).unwrap().1.clone(), index);
            } else if let Some(op) = Forth::find_op(&token) {
                result = op(self)
            } else {
                result = Err(Error::UnknownWord);
            }

            if result.is_ok() {
                result = self.eval_tokens(others);
            }
        }
        result
    }

    fn find_op(token: &str) -> Option<Operation> {
        match token.to_lowercase().as_str() {
            "+" => Some(Forth::add),
            "-" => Some(Forth::sub),
            "*" => Some(Forth::mul),
            "/" => Some(Forth::div),
            "dup" => Some(Forth::dup),
            "drop" => Some(Forth::drop),
            "swap" => Some(Forth::swap),
            "over" => Some(Forth::over),
            _ => None
        }
    }

    fn extract_operands(&mut self, limit: usize) -> Result<Vec<i32>, Error> {
        if self.stack.len() < limit {
            Err(Error::StackUnderflow)
        } else {
            let mut last = Vec::new();
            for _ in 0..limit {
                last.push(self.stack.pop().unwrap())
            }
            Ok(last)
        }
    }

    fn add(&mut self) -> ForthResult {
        self.extract_operands(2)
            .map(|op| {
                self.stack.push(op[1] + op[0]);
            })
    }

    fn sub(&mut self) -> ForthResult {
        self.extract_operands(2)
            .map(|op| {
                self.stack.push(op[1] - op[0]);
            })
    }

    fn mul(&mut self) -> ForthResult {
        self.extract_operands(2)
            .map(|op| {
                self.stack.push(op[1] * op[0]);
            })
    }

    fn div(&mut self) -> ForthResult {
        self.extract_operands(2)
            .and_then(|op| {
                if op[0] == 0 {
                    Err(Error::DivisionByZero)
                } else {
                    self.stack.push(op[1] / op[0]);
                    Ok(())
                }
            })
    }

    fn dup(&mut self) -> ForthResult {
        self.extract_operands(1)
            .map(|op| {
                self.stack.push(op[0]);
                self.stack.push(op[0]);
            })
    }

    fn drop(&mut self) -> ForthResult {
        self.extract_operands(1)
            .map(|_| () )
    }

    fn swap(&mut self) -> ForthResult {
        self.extract_operands(2)
            .map(|op| {
                self.stack.push(op[0]);
                self.stack.push(op[1]);
            })
    }

    fn over(&mut self) -> ForthResult {
        self.extract_operands(2)
            .map(|op| {
                self.stack.push(op[1]);
                self.stack.push(op[0]);
                self.stack.push(op[1]);
            })
    }
}
