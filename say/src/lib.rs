const SMALL: [&str; 14] = [
    "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
    "eleven", "twelve", "thirteen",
];

const MULTIPLES_OF_TEN: [&str; 10] = [
    "", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety",
];

pub fn encode(n: u64) -> String {
    match n {
        0..=13 => SMALL[n as usize].to_string(),
        14 | 16 | 17 | 19 => format!("{}teen", encode(n % 10)),
        15 => "fifteen".to_string(),
        18 => "eighteen".to_string(),
        20|30|40|50|60|70|80|90 => MULTIPLES_OF_TEN[n as usize / 10].to_string(),
        21..=99 => format!("{}-{}", encode(n / 10 * 10), encode(n % 10)),
        _ => match(n as f64).log10() as usize {
            2 => composite(n ,100, "hundred"),
            3..=5 => composite(n ,1_000, "thousand"),
            6..=8 => composite(n ,1_000_000, "million"),
            9..=11 => composite(n ,1_000_000_000,"billion"),
            12..=14 => composite(n ,1_000_000_000_000, "trillion"),
            15..=17 => composite(n ,1_000_000_000_000_000, "quadrillion"),
            _ => composite(n ,1_000_000_000_000_000_000, "quintillion"),
        },
    }
}

fn composite(n: u64, count: u64, name: &str) -> String {
    if n % count == 0 {
        format!("{} {}", encode(n / count), name)
    } else {
        format!("{} {} {}", encode(n / count), name, encode(n % count))
    }
}
