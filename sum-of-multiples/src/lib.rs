pub fn sum_of_multiples(limit: u32, factors: &[u32]) -> u32 {
    (1 .. limit).filter(|n| factors.iter().any(|d| *d != 0 && n % d == 0))
        .sum()
}
