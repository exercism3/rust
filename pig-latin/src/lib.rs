const FINAL_SOUND: &str = "ay";

pub fn translate_word(input: &str) -> String {
    if &input[..2] == "xr" || &input[..2] == "yt" ||
        "aeiou".contains(&input[0..1]) {
        return String::from(input) + FINAL_SOUND;
    }

    let mut post_first = input.chars()
        .position(|c| "aeiouy".contains(c))
        .unwrap();

    if &input[..1] == "y" {
        post_first = 1
    }

    if post_first > 0 && &input[post_first - 1 .. post_first + 1] == "qu" {
        post_first += 1;
    }
    String::from(input[post_first..].to_owned() + &input[0..post_first]) + FINAL_SOUND
}

pub fn translate(input:&str) -> String {
    input.split_whitespace()
        .map(|word| translate_word(word))
        .collect::<Vec<String>>()
        .join(" ")
}

