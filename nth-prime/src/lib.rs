pub fn nth(n: u32) -> u32 {
    (2..).filter(|&x| is_prime(x)).nth(n as usize).unwrap()
}

pub fn is_prime(n: u32) -> bool {
    let sqrt = (n as f64).sqrt() as u32;
    (2..=sqrt).all(|x| n % x != 0)
}
