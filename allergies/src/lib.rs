pub struct Allergies {
    score: u32
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Allergen {
    Eggs,
    Peanuts,
    Shellfish,
    Strawberries,
    Tomatoes,
    Chocolate,
    Pollen,
    Cats,
}

const ALLERGENS: [Allergen; 8] =
    [Allergen::Eggs, Allergen::Peanuts,
     Allergen::Shellfish, Allergen::Strawberries,
     Allergen::Tomatoes, Allergen::Chocolate,
     Allergen::Pollen, Allergen::Cats];

impl Allergies {
    pub fn new(score: u32) -> Self {
        Allergies { score }
    }

    pub fn is_allergic_to(&self, allergen: &Allergen) -> bool {
        // todo!("Determine if the patient is allergic to the '{allergen:?}' allergen.");
        let allergen_position = ALLERGENS.iter().position(|&a| a == *allergen).unwrap();
        self.score & 1 << allergen_position != 0
    }

    pub fn allergies(&self) -> Vec<Allergen> {
        // todo!("Return the list of allergens contained within the score with which the Allergies struct was made.");
        ALLERGENS.iter().filter(|&allergen| self.is_allergic_to(allergen)).cloned().collect()
    }
}
