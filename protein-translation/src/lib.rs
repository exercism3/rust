pub struct CodonsInfo<'a>(Vec<(&'a str, &'a str)>);

impl<'a> CodonsInfo<'a> {
    pub fn name_for(&self, codon: &str) -> Option<&'a str> {
        let index = self.0.binary_search_by_key(&codon, |&(c,_)| c).ok()?;
        Some(self.0[index].1)
    }

    pub fn of_rna(&self, rna: &str) -> Option<Vec<&'a str>> {
        rna.as_bytes().chunks(3)
            .map(|chunk| self.name_for(std::str::from_utf8(chunk).ok()?))
            .take_while(|&protein| protein != Some("stop codon"))
            .collect()
    }
}

pub fn parse<'a>(mut pairs: Vec<(&'a str, &'a str)>) -> CodonsInfo<'a> {
    pairs.sort_by(|&(a, _), &(b, _)| a.cmp(&b));
    CodonsInfo(pairs)
}
