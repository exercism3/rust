
/// A Matcher is a single rule of fizzbuzz: given a function on T, should
/// a word be substituted in? If yes, which word?
pub struct Matcher<T>(Box<dyn Fn(T) -> bool>, String);

impl<T> Matcher<T> {
    pub fn new<F, S>(matcher: F, subs: S) -> Matcher<T> 
    where 
        F: Fn(T) -> bool + 'static,
        String: From<S>,
    {
        Matcher(Box::new(matcher), String::from(subs))
    }
}

/// A Fizzy is a set of matchers, which may be applied to an iterator.
///
/// Strictly speaking, it's usually more idiomatic to use `iter.map()` than to
/// consume an iterator with an `apply` method. Given a Fizzy instance, it's
/// pretty straightforward to construct a closure which applies it to all
/// elements of the iterator. However, we're using the `apply` pattern
/// here because it's a simpler interface for students to implement.
///
/// Also, it's a good excuse to try out using impl trait.
pub struct Fizzy<T>(Vec<Matcher<T>>);

impl<T> Fizzy<T> {
    pub fn new() -> Self {
        Fizzy(Vec::new())
    }

    // feel free to change the signature to `mut self` if you like
    #[must_use]
    pub fn add_matcher(mut self, matcher: Matcher<T>) -> Self {
        self.0.push(matcher);
        self
    }

    /// map this fizzy onto every element of an iterator, returning a new iterator
    pub fn apply<I>(self, iter: I) -> impl Iterator<Item = String>
    where
        I: Iterator<Item = T>,
        T: Copy + ToString,
    {
        iter.map(move |t| {
            let r = self.0.iter().fold(String::new(), |r, Matcher(p, s)|
                if p(t) { r + &s } else { r }
            );
            if r.is_empty() { t.to_string() } else { r }    
        })
    }
}

/// convenience function: return a Fizzy which applies the standard fizz-buzz rules
pub fn fizz_buzz<T>() -> Fizzy<T> 
where
    T: Copy + Rem<Output = T> + PartialEq,
    u8: Into<T>,
{
    Fizzy::new()
        .add_matcher(Matcher::new(|n| n % 3.into() == 0, "fizz"))
        .add_matcher(Matcher::new(|n| n % 5.into() == 0, "buzz"))
}
