/// Check a Luhn checksum.
pub fn is_valid(code: &str) -> bool {
    // todo!("Is the Luhn checksum for {code} valid?");

    let text_without_space = code.replace(" ", "");
    let digits: Vec<u32> = text_without_space.chars()
        .filter_map(|c| c.to_digit(10))
        .collect();

    if digits.len() <= 1 || digits.len() != text_without_space.len() {
        return false;
    }

    let sum: u32 = digits.iter().enumerate()
        .map(|(i, &d)|
            match (digits.len() - i) % 2 {
                // even position from the right
                0 if d * 2 > 9 => d * 2 - 9,
                0 => d * 2,
                // odd position
                _ => d
            }
        ).sum();

    sum % 10 == 0
}

