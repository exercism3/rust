pub fn find_saddle_points(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    input
        .iter()
        .enumerate()
        .flat_map(|(r, vs)| vs.iter().enumerate().map(move |(c, _)| (r, c)))
        .filter(|&(r, c)| input[r][c] == *input[r].iter().max().unwrap())
        .filter(|&(r, c)| input[r][c] == input.iter().map(|vs| vs[c]).min().unwrap())
        .collect()
}
