use std::collections::BTreeSet;

#[derive(Debug, PartialEq, Eq)]
pub struct CustomSet<T>(BTreeSet<T>);

impl<T: Ord + Clone> CustomSet<T> {
    pub fn new(input: &[T]) -> Self {
        Self(input.iter().cloned().collect())
    }

    pub fn contains(&self, element: &T) -> bool {
        self.0.contains(element)
    }

    pub fn add(&mut self, element: T) {
        self.0.insert(element);
    }

    pub fn is_subset(&self, other: &Self) -> bool {
        self.0.is_subset(&other.0)
    }

    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    pub fn is_disjoint(&self, other: &Self) -> bool {
        self.0.is_disjoint(&other.0)
    }

    #[must_use]
    pub fn intersection(&self, other: &Self) -> Self {
        Self(&self.0 & &other.0)
    }

    #[must_use]
    pub fn difference(&self, other: &Self) -> Self {
        Self(&self.0 - &other.0)
    }

    #[must_use]
    pub fn union(&self, other: &Self) -> Self {
        Self(&self.0 | &other.0)
    }
}
