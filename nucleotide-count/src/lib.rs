use std::collections::HashMap;

const NUCLEOTIDES: &str = "ACTG";
pub fn count(nucleotide: char, dna: &str) -> Result<usize, char> {
    dna.chars()
        .chain(std::iter::once(nucleotide))
        .try_for_each(|base|
            if !NUCLEOTIDES.contains(base) {
                Err(base)
            } else {
                Ok(())
            })?;
    Ok(dna.chars().filter(|&base| nucleotide == base).count())
}

pub fn nucleotide_counts(dna: &str) -> Result<HashMap<char, usize>, char> {
    NUCLEOTIDES
        .chars()
        .map(|base| count(base, dna).and_then(|count| Ok((base, count))))
        .collect()
}
