use std::iter::once;

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum Error {
    IncompleteNumber,
}

/// Convert a list of numbers to a stream of bytes encoded with variable length encoding.
pub fn to_bytes(values: &[u32]) -> Vec<u8> {
    values.iter().flat_map(|n| {
        (0..5).map(|i| {
            let r = (*n >> 7 * i) as u8 & 0x7F;
            match i {
                0 => { r },
                _ => { r | 0x80 }
            }
        }).rev().skip_while(|v| *v == 0x80)
    }).collect()
}

/// Given a stream of bytes, extract all numbers which are encoded in there.
pub fn from_bytes(bytes: &[u8]) -> Result<Vec<u32>, Error> {
    bytes.split_inclusive(|&n| {
        n & 0x80 == 0
    }).try_fold(Vec::new(), |res, v| {
        if v.last().ok_or(Error::IncompleteNumber)? & 0x80 == 0 {
            Ok(res.into_iter()
                .chain(once(v.iter().rev().enumerate().map(|(i, &n)| {
                    match i {
                        0 => { (n & 0x7F) as u32 }
                        _ => { (n as u32 & 0x7F) << (i * 7) }
                    }
                }).sum())).collect())
        } else {
            Err(Error::IncompleteNumber)
        }
    })
}
