pub fn abbreviate(phrase: &str) -> String {
    phrase.split(|ch:char| ch.is_whitespace() || ch == '-')
        .map(|word| acronym_of(word))
        .collect::<Vec<String>>()
        .join("")
}

fn acronym_of(word: &str) -> String {
    if word.to_lowercase() == word || word.to_uppercase() == word {
        match word.chars().nth(0) {
            Some(ch) => ch.to_uppercase().to_string(),
            None => "".to_string(),
        }
    } else {
        word.chars()
            .filter(|c| c.is_uppercase())
            .collect::<String>()
    }
}
