#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    SpanTooLong,
    InvalidDigit(char),
}

pub fn lsp(string_digits: &str, span: usize) -> Result<u64, Error> {
    if span > string_digits.len() {
        Err(Error::SpanTooLong)
    } else if let Some(c) = string_digits.chars().find(|c| !c.is_digit(10)) {
        Err(Error::InvalidDigit(c))
    } else if span == 0 {
        Ok(1)
    } else {
        Ok(string_digits
            .chars()
            .map(|c| c.to_digit(10).unwrap() as u64)
            .collect::<Vec<u64>>()
            .windows(span)
            .map(|w| w.iter().product())
            .max()
            .unwrap())
    }
}
