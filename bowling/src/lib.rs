#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    NotEnoughPinsLeft,
    GameComplete,
}

pub struct BowlingGame {
    frames: Vec<u16>,
    roll_again: bool,
}

impl BowlingGame {
    pub fn new() -> Self {
        Self {
            frames: Vec::new(),
            roll_again: false,
        }
    }

    pub fn roll(&mut self, pins: u16) -> Result<(), Error> {
        if pins > 10 || (self.roll_again && pins + self.frames.last().unwrap() > 10) {
            Err(Error::NotEnoughPinsLeft)
        } else if self.score().is_some() {
            Err(Error::GameComplete)
        } else {
            self.frames.push(pins);
            self.roll_again = if pins != 10 {
                !self.roll_again
            } else {
                false
            };
            Ok(())
        }
    }

    pub fn score(&self) -> Option<u16> {
        let mut total = 0;
        let mut frame = 0;

        // as describe the total number of frames is 10
        for _ in 0..10 {
            if let (Some(&first), Some(&second)) =
                (self.frames.get(frame), self.frames.get(frame + 1))
            {
                total += first + second;
                if first == 10 || first + second == 10 {
                    if let Some(&third) = self.frames.get(frame + 2) {
                        total += third;
                    } else {
                        return None;
                    }
                }
                frame += if first == 10 { 1 } else { 2 };
            } else {
                return None;
            }
        }
        Some(total)
    }
}
