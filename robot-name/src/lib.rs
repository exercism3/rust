use std::sync::atomic::{AtomicUsize, Ordering};

static COUNTER: AtomicUsize = AtomicUsize::new(0);
fn get_id() -> usize { COUNTER.fetch_add(1, Ordering::Relaxed) }
pub struct Robot {
    name: String
}

impl Robot {
    pub fn new() -> Self {
        let mut robot = Robot { name: String::new() };
        robot.reset_name();
        robot
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn reset_name(&mut self) {
        let mut new_name: Vec<_> = format!("{:05}", get_id()).chars().collect();
        new_name[0] = ((new_name[0] as u8) - ('0' as u8) + ('A' as u8)) as char;
        new_name[1] = ((new_name[1] as u8) - ('0' as u8) + ('A' as u8)) as char;
        self.name = new_name.into_iter().collect();
    }
}
