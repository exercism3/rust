pub fn is_armstrong_number(num: u32) -> bool {
    // todo!("true if {num} is an armstrong number")
    let number_of_digits = (num as f64).log10() as u32 + 1;
    let mut sum = 0u32;
    let mut temp = num;
    while temp > 0 {
        let cal = (temp % 10).pow(number_of_digits);
        sum = sum.saturating_add(cal);
        temp /= 10;
    }
    num == sum
}
