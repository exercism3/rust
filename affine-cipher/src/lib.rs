/// While the problem description indicates a return status of 1 should be returned on errors,
/// it is much more common to return a `Result`, so we provide an error type for the result here.
#[derive(Debug, Eq, PartialEq)]
pub enum AffineCipherError {
    NotCoprime(i32),
}

/// Encodes the plaintext using the affine cipher with key (`a`, `b`). Note that, rather than
/// returning a return code, the more common convention in Rust is to return a `Result`.
pub fn encode(plaintext: &str, a: i32, b: i32) -> Result<String, AffineCipherError> {
    if !(1..26).any(|n| n * a % 26 == 1) {
        return Err(AffineCipherError::NotCoprime(a));
    }
    let plaintext: String = plaintext.chars().filter(|c| c.is_alphanumeric()).collect();
    let mut result = String::new();
    for (i, c) in plaintext.chars().enumerate() {
        if c.is_alphabetic() {
            let n = c.to_ascii_lowercase() as i32 - 97;
            let e = (n as i32 * a + b) % 26;
            result.push((e + 97) as u8 as char);
        } else {
            result.push(c);
        }
        if i % 5 == 4 && i < plaintext.len() - 1 {
            result.push(' ');
        }
    }
    Ok(result)
}

/// Decodes the ciphertext using the affine cipher with key (`a`, `b`). Note that, rather than
/// returning a return code, the more common convention in Rust is to return a `Result`.
pub fn decode(ciphertext: &str, a: i32, b: i32) -> Result<String, AffineCipherError> {
    let inv = (1..26).find(|n| (a * n) % 26 == 1).ok_or(AffineCipherError::NotCoprime(a))?;
    let mut result = String::new();
    for c in ciphertext.chars() {
        if c.is_alphabetic() {
            let c = c.to_ascii_lowercase();
            let c = c as i32 - 97;
            let c = (inv * (c - b)).rem_euclid(26);
            let c = (c + 97) as u8 as char;
            result.push(c);
        } else if c.is_numeric() {
            result.push(c);
        }
    }
    Ok(result)
}
