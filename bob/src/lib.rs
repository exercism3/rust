pub fn reply(message: &str) -> &str {
    // todo!("have Bob reply to the incoming message: {message}")

    fn all_uppercase(message: &str) -> bool {
        message.chars().any(|ch| ch.is_alphabetic())
            &&
            message.chars()
                .filter(|ch| ch.is_alphabetic())
                .all(|ch| ch.is_uppercase())
    }

    fn is_question(message: &str) -> bool {
        message.trim().ends_with("?")
    }

    fn is_empty(message: &str) -> bool {
        message.trim().is_empty()
    }

    fn message_tuple(message: &str) -> (bool, bool, bool) {
        (all_uppercase(message), is_question(message), is_empty(message))
    }

    match message_tuple(message) {
        (_, _, true) => "Fine. Be that way!",
        (false, true, _) => "Sure.",
        (true, false, _) => "Whoa, chill out!",
        (true, true, _) => "Calm down, I know what I'm doing!",
        _ => "Whatever."
    }
}