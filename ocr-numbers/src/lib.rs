// The code below is a stub. Just enough to satisfy the compiler.
// In order to pass the tests you can add-to or change any of this code.

const CONVERSION_MAP: [(&str, char); 10] = [
    (" _ | ||_|   ", '0'),
    ("     |  |   ", '1'),
    (" _  _||_    ", '2'),
    (" _  _| _|   ", '3'),
    ("   |_|  |   ", '4'),
    (" _ |_  _|   ", '5'),
    (" _ |_ |_|   ", '6'),
    (" _   |  |   ", '7'),
    (" _ |_||_|   ", '8'),
    (" _ |_| _|   ", '9'),
];


#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    InvalidRowCount(usize),
    InvalidColumnCount(usize),
}

pub fn convert(input: &str) -> Result<String, Error> {
    let lines: Vec<&str> = input.lines().collect();
    if lines.len() % 4 != 0 {
        return Err(Error::InvalidRowCount(lines.len()));
    }
    if lines.iter().any(|line| line.len() % 3 != 0) {
        return Err(Error::InvalidColumnCount(lines[0].len()));
    }

    let map = CONVERSION_MAP.iter().cloned().collect::<std::collections::HashMap<_, _>>();
    let mut result = String::with_capacity(lines.len() / (4 * 3));

    for n_row in lines.chunks(4) {
        for col in (0..n_row[0].len()).step_by(3) {
            let number = n_row
                .iter()
                .flat_map(|line| line.chars().skip(col).take(3))
                .collect::<String>();

            result.push(*map.get(number.as_str()).unwrap_or(&'?'));
        }
        result.push(',');
    }
    result.pop();
    Ok(result)
}
