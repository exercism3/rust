use std::collections::HashMap;

/// `InputCellId` is a unique identifier for an input cell.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct InputCellId(usize);

/// `ComputeCellId` is a unique identifier for a compute cell.
/// Values of type `InputCellId` and `ComputeCellId` should not be mutually assignable,
/// demonstrated by the following tests:
///
/// ```compile_fail
/// let mut r = react::Reactor::new();
/// let input: react::ComputeCellId = r.create_input(111);
/// ```
///
/// ```compile_fail
/// let mut r = react::Reactor::new();
/// let input = r.create_input(111);
/// let compute: react::InputCellId = r.create_compute(&[react::CellId::Input(input)], |_| 222).unwrap();
/// ```
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct ComputeCellId(usize);

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct CallbackId(usize);

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CellId {
    Input(InputCellId),
    Compute(ComputeCellId),
}

#[derive(Debug, PartialEq)]
pub enum RemoveCallbackError {
    NonexistentCell,
    NonexistentCallback,
}

struct ComputeCell<'a, T> {
    func: Box<dyn 'a + Fn(&[T]) -> T>,
    dependencies: Vec<CellId>,
    callbacks: HashMap<CallbackId, Box<dyn 'a + FnMut(T)>>,
}

impl<'a, T> ComputeCell<'a, T>
    where
        T: Copy + PartialEq + Default,
{
    fn new<F: 'a + Fn(&[T]) -> T>(func: F, dependencies: &[CellId]) -> Self {
        Self {
            func: Box::new(func),
            dependencies: dependencies.iter().copied().collect(),
            callbacks: HashMap::new(),
        }
    }

    fn value(&self, reactor: &Reactor<'a, T>) -> T {
        let args = self
            .dependencies
            .iter()
            .map(|&id| reactor.value(id).unwrap())
            .collect::<Vec<_>>();
        (*self.func)(args.as_ref())
    }

    fn add_callback<F: 'a + FnMut(T)>(&mut self, id: CallbackId, callback: F) {
        self.callbacks.insert(id, Box::new(callback));
    }

    fn remove_callback(&mut self, id: CallbackId) -> Result<(), RemoveCallbackError> {
        self.callbacks
            .remove(&id)
            .ok_or(RemoveCallbackError::NonexistentCallback)
            .map(|_| ())
    }

    fn fire_callback(&mut self, value: T) {
        for callback in self.callbacks.values_mut() {
            callback(value);
        }
    }
}

#[derive(Default)]
pub struct Reactor<'a, T> {
    // Just so that the compiler doesn't complain about an unused type parameter.
    // You probably want to delete this field.
    current_id: usize,
    input_values: HashMap<InputCellId, T>,
    compute_cells: HashMap<ComputeCellId, ComputeCell<'a, T>>,
}


// You are guaranteed that Reactor will only be tested against types that are Copy + PartialEq.
impl<'a, T> Reactor<'a, T>
    where
        T: Copy + PartialEq + Default {
    pub fn new() -> Self {
        Default::default()
    }

    // Creates an input cell with the specified initial value, returning its ID.
    pub fn create_input(&mut self, initial: T) -> InputCellId {
        let input_id = InputCellId(self.next_id());
        self.input_values.insert(input_id, initial);
        input_id
    }

    fn next_id(&mut self) -> usize {
        self.current_id += 1;
        self.current_id
    }

    // Creates a compute cell with the specified dependencies and compute function.
    // The compute function is expected to take in its arguments in the same order as specified in
    // `dependencies`.
    // You do not need to reject compute functions that expect more arguments than there are
    // dependencies (how would you check for this, anyway?).
    //
    // If any dependency doesn't exist, returns an Err with that nonexistent dependency.
    // (If multiple dependencies do not exist, exactly which one is returned is not defined and
    // will not be tested)
    //
    // Notice that there is no way to *remove* a cell.
    // This means that you may assume, without checking, that if the dependencies exist at creation
    // time they will continue to exist as long as the Reactor exists.
    pub fn create_compute<F: 'a + Fn(&[T]) -> T>(
        &mut self,
        dependencies: &[CellId],
        compute_func: F,
    ) -> Result<ComputeCellId, CellId> {
        if let Some(id) = dependencies.iter().find(|id| self.value(**id).is_none()) {
            return Err(*id);
        }
        let compute_id = ComputeCellId(self.next_id());
        self.compute_cells
            .insert(compute_id, ComputeCell::new(compute_func, dependencies));
        Ok(compute_id)
    }

    // Retrieves the current value of the cell, or None if the cell does not exist.
    //
    // You may wonder whether it is possible to implement `get(&self, id: CellId) -> Option<&Cell>`
    // and have a `value(&self)` method on `Cell`.
    //
    // It turns out this introduces a significant amount of extra complexity to this exercise.
    // We chose not to cover this here, since this exercise is probably enough work as-is.
    pub fn value(&self, id: CellId) -> Option<T> {
        match id {
            CellId::Input(input_id) => self.input_values.get(&input_id).cloned(),
            CellId::Compute(compute_id) => self
                .compute_cells
                .get(&compute_id)
                .map(|cell| cell.value(self)),
        }
    }

    // Sets the value of the specified input cell.
    //
    // Returns false if the cell does not exist.
    //
    // Similarly, you may wonder about `get_mut(&mut self, id: CellId) -> Option<&mut Cell>`, with
    // a `set_value(&mut self, new_value: T)` method on `Cell`.
    //
    // As before, that turned out to add too much extra complexity.
    pub fn set_value(&mut self, id: InputCellId, new_value: T) -> bool {
        if !self.input_values.contains_key(&id) {
            return false;
        }
        let old_values = self
            .compute_cells
            .iter()
            .map(|(&compute_id, cell)| (compute_id, cell.value(self)))
            .collect::<Vec<_>>();
        if let Some(x) = self.input_values.get_mut(&id) {
            *x = new_value;
        };
        for (compute_id, old_value) in old_values {
            let cell = self.compute_cells.get(&compute_id).unwrap();
            let new_value = cell.value(self);
            if new_value != old_value {
                let cell = self.compute_cells.get_mut(&compute_id).unwrap();
                cell.fire_callback(new_value);
            }
        }
        true
    }

    pub fn add_callback<F: 'a + FnMut(T)>(
        &mut self,
        id: ComputeCellId,
        callback: F,
    ) -> Option<CallbackId> {
        let callback_id = CallbackId(self.next_id());
        self.compute_cells
            .get_mut(&id)?
            .add_callback(callback_id, callback);
        Some(callback_id)
    }

    pub fn remove_callback(
        &mut self,
        cell_id: ComputeCellId,
        callback_id: CallbackId,
    ) -> Result<(), RemoveCallbackError> {
        self.compute_cells
            .get_mut(&cell_id)
            .ok_or(RemoveCallbackError::NonexistentCell)
            .and_then(|cell| cell.remove_callback(callback_id))
    }
}

