const STUDENTS: [&str; 12] = ["Alice", "Bob", "Charlie", "David",
    "Eve", "Fred", "Ginny", "Harriet",
    "Ileana", "Joseph", "Kincaid", "Larry"];

pub fn plants(diagram: &str, student: &str) -> Vec<&'static str> {
    // todo!("based on the {diagram}, determine the plants the {student} is responsible for");
    let plan_start_pos = STUDENTS.iter().position(|&name| name.eq(student)).unwrap() * 2;

    diagram
        .lines()
        .flat_map(|line| {
            line[plan_start_pos..plan_start_pos + 2].chars()
                .map(|ch| match ch {
                    'G' => "grass",
                    'C' => "clover",
                    'R' => "radishes",
                    'V' => "violets",
                    _ => "unknown"
                })
        })
        .collect()
}