#[derive(Debug, PartialEq, Eq)]
pub enum Classification {
    Abundant,
    Perfect,
    Deficient,
}

pub fn classify(num: u64) -> Option<Classification> {
    match (1..=if num > 1 {num/2} else {num})
        .filter(|&n| (num % n == 0) && (num != n))
        .sum() {
            0_u64 if num == 0 => None,
            s if s < num => Some(Classification::Deficient),
            s if s > num => Some(Classification::Abundant),
            _ => Some(Classification::Perfect),
    }
}
