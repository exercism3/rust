use std::io::{Read, Write};
macro_rules! io_stats {
    ($tname:ident, $bound:path) => {
        pub struct $tname<T> {
            wrapped: T,
            num_bytes: usize,
            num_ops: usize,
        }

        impl<T: $bound> $tname<T> {
            pub fn new(wrapped: T) -> Self {
                Self {
                    wrapped,
                    num_bytes: 0,
                    num_ops: 0,
                }
            }

            pub fn get_ref(&self) -> &T {
                &self.wrapped
            }

            pub fn bytes_through(&self) -> usize {
                self.num_bytes
            }
        }
    };
}

io_stats!(ReadStats, Read);
io_stats!(WriteStats, Write);

impl<R: Read> ReadStats<R> {
    pub fn reads(&self) -> usize {
        self.num_ops
    }
}

impl<R: Read> Read for ReadStats<R> {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        self.num_ops += 1;
        let result = self.wrapped.read(buf)?;
        self.num_bytes += result;
        Ok(result)
    }
}

impl<W: Write> WriteStats<W> {
    pub fn writes(&self) -> usize {
        self.num_ops
    }
}

impl<W: Write> Write for WriteStats<W> {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        self.num_ops += 1;
        let result = self.wrapped.write(buf)?;
        self.num_bytes += result;
        Ok(result)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        self.wrapped.flush()
    }
}
