use std::vec;

pub struct RailFence(usize);

impl RailFence {
    pub fn new(rails: u32) -> RailFence {
        Self(rails as usize)
    }

    pub fn encode(&self, text: &str) -> String {
        let mut rails = vec![String::new(); self.0];
        for c in text.chars().zip(self.zigzag().take(text.len())) {
            rails[c.1].push(c.0);
        }
        rails.into_iter().collect()
    }

    pub fn decode(&self, cipher: &str) -> String {
        let mut indices: Vec<_> = self.zigzag().zip(0..).take(cipher.len()).collect();
        indices.sort();

        let mut chars:Vec<_> = indices
            .into_iter()
            .map(|(r,i)| (i,r))
            .zip(cipher.chars())
            .collect();
        chars.sort();

        chars.into_iter().map(|((_, _), c)| c).collect()
    }

    fn zigzag(&self) -> impl Iterator<Item = usize> {
        (0..self.0).chain((1..self.0 - 1).rev()).cycle()
    }
}
