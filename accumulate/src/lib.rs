/// What should the type of _function be?
pub fn map<F,T,R>(input: Vec<T>, function: F) -> Vec<R>
where F: FnMut(T) -> R {
    input.into_iter().map(function).collect()
}
