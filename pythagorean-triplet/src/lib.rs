use std::collections::HashSet;

pub fn find(sum: u32) -> HashSet<[u32; 3]> {
    let limit = sum /2;
    (1..=limit)
        .flat_map(|i| {
            (i..=limit)
                .filter(move |&j| i * i + j * j == (sum - i - j) * (sum - i - j))
                .map(move |j| [i, j, sum - i - j])  
        })
        .collect()
}
