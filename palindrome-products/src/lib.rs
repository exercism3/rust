/// `Palindrome` is a newtype which only exists when the contained value is a palindrome number in base ten.
///
/// A struct with a single field which is used to constrain behavior like this is called a "newtype", and its use is
/// often referred to as the "newtype pattern". This is a fairly common pattern in Rust.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Palindrome(u64);

impl Palindrome {
    /// Create a `Palindrome` only if `value` is in fact a palindrome when represented in base ten. Otherwise, `None`.
    pub fn new(value: u64) -> Option<Palindrome> {
        let mut value_fwd = value;
        let mut value_rev = 0;
        while value_fwd > 0 {
            value_rev *= 10;
            value_rev += value_fwd % 10;
            value_fwd /= 10;
        }
        (value == value_rev).then_some(Palindrome(value))
    }

    /// Get the value of this palindrome.
    pub fn into_inner(self) -> u64 {
        self.0
    }
}

pub fn palindrome_products(min: u64, max: u64) -> Option<(Palindrome, Palindrome)> {
    let mut min_prod: Option<Palindrome> = None;
    let mut max_prod: Option<Palindrome> = None;
    for a in min..max {
        for b in min..=max {
            let Some(p) = Palindrome::new(a * b) else {
                continue;
            };
            println!("{p:?}");
            min_prod = match min_prod {
                Some(min) if min.into_inner() < p.into_inner() => min_prod,
                _ => Some(p),
            };
            println!("{min_prod:?}");
            max_prod = match max_prod {
                Some(max) if max.into_inner() > p.into_inner() => max_prod,
                _ => Some(p),
            };
        }
    }
    Some((min_prod?, max_prod?))
}
