use std::collections::VecDeque;

pub struct CircularBuffer<T> {
    capacity: usize,
    queue: VecDeque<T>,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    EmptyBuffer,
    FullBuffer,
}

impl<T> CircularBuffer<T> {
    pub fn new(capacity: usize) -> Self {
        Self {
            capacity,
            queue: VecDeque::with_capacity(capacity),
        }
    }

    pub fn write(&mut self, _element: T) -> Result<(), Error> {
        match self.queue.len() {
            len if len < self.capacity => {
                self.queue.push_back(_element);
                Ok(())
            }
            _ => Err(Error::FullBuffer),
        }
    }

    pub fn read(&mut self) -> Result<T, Error> {
        match self.queue.pop_front() {
            Some(element) => Ok(element),
            None => Err(Error::EmptyBuffer),
        }
    }

    pub fn clear(&mut self) {
        self.queue.clear();
    }

    pub fn overwrite(&mut self, element: T) {
        if self.queue.len() == self.capacity {
            self.queue.pop_front();
        }
        self.write(element).unwrap()
    }
}
