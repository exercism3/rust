use rand::Rng;

fn process_str<F>(key: &str, s: &str, func: F) -> Option<String>
where
    F: FnOnce((u8, u8)) -> char + Copy,
{
    (key.len() != 0).then_some(()).and_then(|_| {
        s.as_bytes()
            .into_iter()
            .zip(key.as_bytes().iter().cycle())
            .map(|(&s1, &s2)| match (s1, s2) {
                (s1 @ b'a'..=b'z', s2 @ b'a'..=b'z') => Some((s1, s2)),
                _ => return None,
            })
            .map(|pair| pair.map(func))
            .collect()
    })
}

pub fn encode(key: &str, s: &str) -> Option<String> {
    process_str(key, s, |pair| {
        ((pair.0 as u8 - b'a' + pair.1 as u8 - b'a').rem_euclid(26) + b'a') as char
    })
}

pub fn decode(key: &str, s: &str) -> Option<String> {
    process_str(key, s, |pair| {
        ((pair.0 as i16 - pair.1 as i16 + 26).rem_euclid(26) as u8 + b'a') as char
    })
}

pub fn encode_random(s: &str) -> (String, String) {
    let mut rng = rand::thread_rng();
    let key: String = (0..100)
        .map(|_| (rng.gen_range(b'a'..=b'z')) as char)
        .collect();
    let encoded = encode(&key, s).unwrap();
    (key, encoded)
}
