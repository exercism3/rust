pub fn number(user_number: &str) -> Option<String> {
    let mut user_number: String = user_number.chars().filter(|c| c.is_digit(10)).collect::<String>();
    if user_number.starts_with("1") {
        user_number.remove(0);
    }
    let first = user_number.chars().nth(0)?.to_digit(10)?;
    let forth = user_number.chars().nth(3)?.to_digit(10)?;
    if first < 2 || forth < 2 || user_number.len() != 10 {
        return None;
    }
    return Some(user_number);
}
