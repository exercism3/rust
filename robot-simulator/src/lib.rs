// The code below is a stub. Just enough to satisfy the compiler.
// In order to pass the tests you can add-to or change any of this code.
use crate::Direction::{North, East, South, West};
#[derive(PartialEq, Eq, Debug, Copy, Clone)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

pub struct Robot {
    pos: (i32, i32),
    dir: Direction,
}

const DIRECTIONS: [Direction; 4] = [North, East, South, West];

impl Robot {
    pub fn new(x: i32, y: i32, dir: Direction) -> Self {
        Self {
            pos: (x, y),
            dir,
        }
    }

    #[must_use]
    pub fn turn_right(self) -> Self {
        Robot { dir: DIRECTIONS[(self.dir as isize + 1).rem_euclid(4) as usize], ..self }
    }

    #[must_use]
    pub fn turn_left(self) -> Self {
        Robot { dir: DIRECTIONS[(self.dir as isize - 1).rem_euclid(4) as usize], ..self }
    }

    #[must_use]
    pub fn advance(self) -> Self {
        match self.dir {
            Direction::North => Robot { pos: (self.pos.0, self.pos.1 + 1), ..self },
            Direction::East => Robot { pos: (self.pos.0 + 1, self.pos.1), ..self },
            Direction::South => Robot { pos: (self.pos.0, self.pos.1 - 1), ..self },
            Direction::West => Robot { pos: (self.pos.0 - 1, self.pos.1), ..self },
        }
    }

    #[must_use]
    pub fn instructions(mut self, instructions: &str) -> Self {
        for instruction in instructions.chars() {
            match instruction {
                'L' => self = self.turn_left(),
                'R' => self = self.turn_right(),
                _ => self = self.advance(),
            };
        }
        self
    }

    pub fn position(&self) -> (i32, i32) {
        self.pos
    }

    pub fn direction(&self) -> &Direction {
        &self.dir
    }
}
