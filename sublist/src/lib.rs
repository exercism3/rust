
#[derive(Debug, PartialEq, Eq)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

pub fn sublist<T: PartialEq>(_first_list: &[T], _second_list: &[T]) -> Comparison {
    // todo!("Determine if the first list is equal to, sublist of, superlist of or unequal to the second list.");
    fn find_window<T:PartialEq>(a:&[T], b:&[T]) -> bool {
        a.is_empty() || b.windows(a.len()).any(|w| a == w)
    }

    if _first_list.len() == _second_list.len() && _first_list == _second_list {
        Comparison::Equal
    } else if _first_list.len() < _second_list.len() && find_window(_first_list, _second_list) {
        Comparison::Sublist
    } else if find_window(_second_list, _first_list) {
        Comparison::Superlist
    } else {
        Comparison::Unequal
    }
}
