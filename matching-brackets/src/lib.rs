pub fn brackets_are_balanced(string: &str) -> bool {
    // Stack of opening chars
    let mut open = Vec::new();

    for c in string.chars() {
        match c {
            '[' => open.push(']'),
            '{' => open.push('}'),
            '(' => open.push(')'),

            ']' | '}' | ')' => {
                if open.pop() != Some(c) {
                    return false
                }
            }

            _ => (),  // Ignored
        }
    }

    open.is_empty()
}
