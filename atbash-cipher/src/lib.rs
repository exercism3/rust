
/// "Encipher" with the Atbash cipher.
pub fn encode(plain: &str) -> String {
    decode(
        plain
        .to_lowercase()
        .chars().filter(|c| c.is_alphanumeric())
        .collect::<String>().as_str()
    ).chars().collect::<Vec<char>>()
    .chunks(5).collect::<Vec<_>>()
    .join(&[' '][..]).iter().collect()
}

/// "Decipher" with the Atbash cipher.
pub fn decode(cipher: &str) -> String {
    cipher.chars().filter(|&c| c!= ' ').map(|c|
        if c.is_digit(10) {
            c
        } else {
            ('z' as u8 - c as u8 + 'a' as u8) as char
        }
    ).collect()
}
