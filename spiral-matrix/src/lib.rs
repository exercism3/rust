pub fn spiral_matrix(size: u32) -> Vec<Vec<u32>> {
    let size = size as usize;
    let mut matrix = vec![vec![0; size]; size];
    let mut l = size + 1;
    let mut n = 1..;
    for i in 0..(size + 1) / 2 {
        let mut x = i;
        let mut y = i;
        l -= 2;
        if l == 0 {
            matrix[y][x] = n.next().unwrap();
        } else {
            for (dx, dy) in [(1, 0), (0, 1), (-1, 0), (0, -1)].iter() {
                for _ in 0..l {
                    matrix[y][x] = n.next().unwrap();
                    x = (x as i32 + dx) as usize;
                    y = (y as i32 + dy) as usize;
                }
            }
        }
    }
    matrix
}
